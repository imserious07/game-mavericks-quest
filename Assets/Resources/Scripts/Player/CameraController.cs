﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public float lookSmooth = 0.09f;
    public Vector3 offsetFromTarget = new Vector3(0, 6, -8);
    public float xTilt = 10;

    Vector3 destination = Vector3.zero;
    CharacterController charController;
    float rotateVelocity = 0;

	void Start () {
        SetCameraTarget(target);
	}

    // Данная процедура позволит переключать камеру между целями
    void SetCameraTarget(Transform t) {
        target = t;

        if (target != null) {
            if (target.GetComponent<CharacterController>()) {
                charController = target.GetComponent<CharacterController>();
            } else {
                Debug.LogError("У цели камеры не привязан скрипт CharacterController!");
            }
        } else {
            Debug.LogError("У камеры не указана цель!");
        }
    }

    // Для того чтобы код сработал после смещения персонажа
	void LateUpdate () {
        MoveToTarget();
        LookAtTarget();
	}

    void MoveToTarget() {
        destination = charController.TargetRotation * offsetFromTarget;
        destination += target.position;
        transform.position = destination;
    }

    void LookAtTarget() {
        float eulerYAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y,
                                                  target.eulerAngles.y,
                                                  ref rotateVelocity,
                                                  lookSmooth);
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x,
                                              eulerYAngle,
                                              0);
    }
}
