﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class CharacterController : MonoBehaviour {

    [System.Serializable]
    public class MoveSettings {
        public float rotateVelocity = 100;
        public float forwardVelocity = 12;
        public float jumpVel = 25;
        public float distToGrounded = 0.1f;
        public LayerMask ground;
    }

    [System.Serializable]
    public class PhysSettings {
        public float downAccel = 0.75f;
    }

    [System.Serializable]
    public class InputSettings {
        public float inputDelay = 0.1f;
        public string FORWARD_AXIS = "Vertical";
        public string TURN_AXIS = "Horizontal";
        public string JUMP_AXIS = "Jump";
    }

    public Text UIGameOver;
    public Canvas UIJoystick;

    public MoveSettings moveSettings = new MoveSettings();
    public PhysSettings physSettings = new PhysSettings();
    public InputSettings inputSettings = new InputSettings();

    Vector3 velocity = Vector3.zero;

    Quaternion targetRotation;
    Rigidbody rBody;
    Animator anim;
    Text UIscore;
    float forwardInput, turnInput, jumpInput;
    int coins = 0;

    public Quaternion TargetRotation {
        get { return targetRotation; }
    }

    bool Grounded() {
        return Physics.Raycast(transform.position,
                               Vector3.down,
                               moveSettings.distToGrounded,
                               moveSettings.ground);
    }

	void Start () {
        targetRotation = transform.rotation;
        if (GetComponent<Rigidbody>()) {
            rBody = GetComponent<Rigidbody>();
        } else {
            Debug.LogError("Rigidbody не привязан к игроку!");
        }

        if (GetComponent<Animator>()) {
            anim = GetComponent<Animator>();
        } else {
            Debug.LogError("Animator не привязан к игроку");
        }

        forwardInput = turnInput = jumpInput = 0;

        UIscore = GameObject.FindWithTag("UIscore").GetComponent<Text>();
    }

    void GetInput() {
        if (Application.platform == RuntimePlatform.Android ||
            Application.platform == RuntimePlatform.WindowsEditor) {
            // дробное число от -1 до 1
            forwardInput = CrossPlatformInputManager.GetAxis(inputSettings.FORWARD_AXIS);
            turnInput = CrossPlatformInputManager.GetAxis(inputSettings.TURN_AXIS);
            // только целые числа -1 0 1
            if (CrossPlatformInputManager.GetButton("Jump")) jumpInput = 1; else jumpInput = 0;
        } else {
            // дробное число от -1 до 1
            forwardInput = Input.GetAxis(inputSettings.FORWARD_AXIS);
            turnInput = Input.GetAxis(inputSettings.TURN_AXIS);
            // только целые числа -1 0 1
            jumpInput = Input.GetAxisRaw(inputSettings.JUMP_AXIS);
        }
    }

	void Update () {
        GetInput();
        Turn();
        Jump();
	}

    void FixedUpdate() {
        Run();
        Jump();

        rBody.velocity = transform.TransformDirection(velocity);
    }

    void Run() {
        if (Mathf.Abs(forwardInput) > inputSettings.inputDelay) {
            // Движение
            velocity.z = moveSettings.forwardVelocity * forwardInput;
            // Для анимации
            anim.SetFloat("MovementSpeed", Mathf.Abs(forwardInput));
        } else {
            // Джойстик не активен
            velocity.z = 0;
            anim.SetFloat("MovementSpeed", 0);
        }
    }

    void Turn() {
        if (Mathf.Abs(turnInput) > inputSettings.inputDelay) {
            // Поворот
            targetRotation *= Quaternion.AngleAxis(moveSettings.rotateVelocity * turnInput * Time.deltaTime, Vector3.up);
            transform.rotation = targetRotation;
        } else {
            // Джойстик не активен
            rBody.velocity = Vector3.zero;
        }
    }

    void Jump() {
        if (jumpInput > 0 && Grounded()) {
            // Прыжок
            velocity.y = moveSettings.jumpVel;
            // Для анимации
            anim.SetBool("isGrounded", false);
        } else if (jumpInput == 0 && Grounded()) {
            // Чтобы не уйти под землю
            velocity.y = 0;
            // Для анимации
            anim.SetBool("isGrounded", true);
        } else {
            // Падение
            velocity.y -= physSettings.downAccel;
        }
    }

    // Для поднятия монет
    void OnTriggerEnter(Collider col) {
        GameObject coin = col.gameObject;
        if (coin.tag == "coin") {
            Destroy(coin);
            coins++;
            UIscore.text = "Coins: " + coins;
        } else if (coin.tag == "exit") {
            UIControl.HideUI("Score: " + (coins * 100));
        }
    }
}
