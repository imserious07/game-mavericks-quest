﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {

    public static Canvas Controls;
    public static Text UIGameOver;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void HideUI(string title) {
        UIGameOver.text = title;
        Destroy(GameObject.FindGameObjectWithTag("timer"));
        Destroy(GameObject.FindGameObjectWithTag("UIscore"));
        Destroy(Controls.gameObject);
    }
}
