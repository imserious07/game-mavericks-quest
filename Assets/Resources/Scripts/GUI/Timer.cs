﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public int timeLeft = 60;
    
    
    float currentTimer = 1f;
    Text UItimer;

    void Start() {
        initUIControl();

        UItimer = GetComponent<Text>();
        UIControl.UIGameOver.text = "";
    }

    void initUIControl() {
        UIControl.UIGameOver = GameObject.FindGameObjectWithTag("gameover").GetComponent<Text>();
        UIControl.Controls = GameObject.FindGameObjectWithTag("control").GetComponent<Canvas>();
    }

	void Update () {
        if (timeLeft != 0) {
            currentTimer -= Time.deltaTime;

            if (currentTimer <= 0) {
                currentTimer = 1f;
                timeLeft--;
                UItimer.text = timeLeft.ToString();
            }

            if (timeLeft == 0) {
                TimeOut();
            }
        }
    }

    void TimeOut() {
        UIControl.HideUI("Game over");
    }
}
