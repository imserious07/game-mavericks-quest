﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour {
    public Terrain terrain;
    public int numberOfObjects; // Сколько объектов необходимо создать
    private int currentObjects; // Сколько объектов уже создано
    public GameObject objectToPlace; // Префаб объекта
    private int terrainWidth; // Ширина ландшафта
    private int terrainLength; // Длинна ландшафта
    private int terrainPosX; // X позиция ландшафта
    private int terrainPosZ; // Z позиция ландшафта
    void Start() {
        terrainWidth = (int)terrain.terrainData.size.x;
        terrainLength = (int)terrain.terrainData.size.z;
        terrainPosX = (int)terrain.transform.position.x;
        terrainPosZ = (int)terrain.transform.position.z;

        GenerateCoins();
    }

    // Создание монет
    void GenerateCoins() {
        while (currentObjects <= numberOfObjects) {
            // Выбор случайной позиции по X
            int posx = Random.Range(terrainPosX, terrainPosX + terrainWidth);
            // Выбор случайной позиции по Z
            int posz = Random.Range(terrainPosZ, terrainPosZ + terrainLength);
            // Получаем высоту по координатам X Z
            float posy = Terrain.activeTerrain.SampleHeight(new Vector3(posx, 0, posz));
            // Создаём новый объект на выбранной случайной позиции
            Vector3 defRotation = new Vector3(90, 0, 0);
            GameObject newObject = (GameObject)Instantiate(objectToPlace,
                                                           new Vector3(posx, posy + 2, posz),
                                                           Quaternion.Euler(defRotation));
            currentObjects += 1;
            if (currentObjects == numberOfObjects) {
                Debug.Log("Все монеты размещены!");
            }
        }
    }
}
