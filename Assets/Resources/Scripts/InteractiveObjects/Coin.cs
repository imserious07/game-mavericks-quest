﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public float rotationSpeed = 0.5f;

	void Update () {
        transform.Rotate(0, 0, rotationSpeed);
	}
}
